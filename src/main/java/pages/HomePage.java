package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.Random;

public class HomePage {

    @FindBy(xpath = "//i[@class='fa fa-user']")
    private WebElement myAccountDropDown;

    @FindBy(xpath = "/html[1]/body[1]/nav[1]/div[1]/div[2]/ul[1]/li[2]/ul[1]/li[2]/a[1]")
    private WebElement login;

    @FindBy(xpath = "/html[1]/body[1]/nav[1]/div[1]/div[2]/ul[1]/li[2]/ul[1]/li[5]/a[1]")
    private WebElement logout;

    @FindBy(xpath = "//ul[@class='nav navbar-nav']")
    private WebElement navigationBar;

    public WebElement selectRandomCategory() {
        Random random = new Random();
        int randomCategory;
        List<WebElement> allFromPanel = navigationBar.findElements(By.xpath("*"));
        randomCategory = random.nextInt(allFromPanel.size());
        WebElement category = allFromPanel.get(randomCategory);
        return category;
    }

    public WebElement getMyAccountDropDown() {
        return myAccountDropDown;
    }

    public WebElement getLogin() {
        return login;
    }

    public WebElement getLogout() {
        return logout;
    }

    public WebElement getNavigationBar() {
        return navigationBar;
    }
}
