package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MyAccountPage {

    @FindBy(xpath = "/html[1]/body[1]/header[1]/div[1]/div[1]/div[1]/div[1]/h1[1]/a[1]")
    private WebElement yourStore;

    @FindBy(xpath = "/html[1]/body[1]/div[2]/div[1]/div[1]/h1[1]")
    private WebElement loggedOutSuccess;

    public WebElement getYourStore() {
        return yourStore;
    }

    public WebElement getLoggedOutSuccess() {
        return loggedOutSuccess;
    }
}
