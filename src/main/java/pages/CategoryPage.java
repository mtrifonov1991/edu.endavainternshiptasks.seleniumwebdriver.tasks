package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.Random;

public class CategoryPage {

    @FindBy(className = "image")
    private WebElement product;

    @FindBy(className = "see-all")
    private WebElement showAll;

    @FindBy(xpath = "/html[1]/body[1]/div[2]/div[1]/div[1]/div[1]/div[1]/a[1]")
    private WebElement continueButton;

//    public WebElement selectRandomProduct() {
//        Random random = new Random();
//        int randomProduct;
//        List<WebElement> allProducts = product.findElements(By.xpath("*"));
//        randomProduct = random.nextInt(allProducts.size());
//        WebElement product = allProducts.get(randomProduct);
//        return product;
//
//    }

    public WebElement getProduct() {
        return product;
    }

    public WebElement getShowAll() {
        return showAll;
    }

    public WebElement getContinueButton() {
        return continueButton;
    }
}
