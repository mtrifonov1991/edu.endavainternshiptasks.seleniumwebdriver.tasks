package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ProductPage {

    @FindBy(id = "button-cart")
    private WebElement addToCartButton;

    @FindBy(xpath = "//span[@id='cart-total']")
    private WebElement checkCart;

    @FindBy(xpath = "/html[1]/body[1]/header[1]/div[1]/div[1]/div[3]/div[1]/ul[1]/li[2]/div[1]/p[1]/a[1]/strong[1]")
    private WebElement viewCart;

    public WebElement getAddToCartButton() {
        return addToCartButton;
    }

    public WebElement getCheckCart() {
        return checkCart;
    }

    public WebElement getViewCart() {
        return viewCart;
    }
}
