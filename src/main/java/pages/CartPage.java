package pages;

import openCartBot.WebDriverConfiguration;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CartPage {

    @FindBy(xpath = "//table[@class='table table-bordered']//a[contains(text(),'MacBook')]")
    private WebElement productInCart;

    @FindBy(xpath = "//button[@class='btn btn-danger']")
    private WebElement deleteButton;

    @FindBy(xpath = "//p[@class='text-center']")
    private WebElement emptyCard;

    @FindBy(xpath = "//td[4]//div[1]//span[1]//button[2]")
    private WebElement moreThan1ProductInCart;

    public WebElement getProductInCart() {
        return productInCart;
    }

    public WebElement getDeleteButton() {
        return deleteButton;
    }

    public WebElement getEmptyCard() {
        return emptyCard;
    }

    public WebElement getMoreThan1ProductInCart() {
        return moreThan1ProductInCart;
    }
}
