package openCartBot;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.Random;

//   Tema pe acasa :
//  Log in, add to cart, check product is in cart
//  Log in, Add to cart, remove from cart, check cart empty
//  Log in, Add multiple, remove one, check removed

public class WebDriverConfiguration {
    static WebDriver myDriver;
    static final String WebSite = "https://demo.opencart.com";

    public static WebDriverConfiguration webDriverConfiguration;

    public WebDriverConfiguration(){
        if (webDriverConfiguration != null) {
            webDriverConfiguration = new WebDriverConfiguration();
        }
    }

    @Before
    public void startWebDriver() {
        System.setProperty("webdriver.chrome.driver",
                "C:\\Users\\mtrifonov\\LOCAL BITBUKET REPOSITORIES\\edu.Endava.InternshipTasks." +
                        "SeleniumWebDriver.Task1\\src\\main\\resources\\chromedriver.exe");
        myDriver = new ChromeDriver();
    }

    public WebDriver getMyDriver() {
        return myDriver;
    }


    @After
    public void closeWebDriver() {
        myDriver.close();
        myDriver.quit();
    }
}
