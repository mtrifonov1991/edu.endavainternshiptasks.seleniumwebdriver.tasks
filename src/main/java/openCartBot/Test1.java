package openCartBot;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import pages.*;


import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

//  Log in, add to cart, check product is in cart

public class Test1 extends WebDriverConfiguration {
    @Test
    public void run() throws InterruptedException {
        myDriver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        myDriver.get(WebSite);

        HomePage homePage = PageFactory.initElements(myDriver, HomePage.class);
        LoginPage loginPage = PageFactory.initElements(myDriver, LoginPage.class);
        MyAccountPage myAccountPage = PageFactory.initElements(myDriver, MyAccountPage.class);
        CategoryPage categoryPage = PageFactory.initElements(myDriver, CategoryPage.class);
        ProductPage productPage = PageFactory.initElements(myDriver, ProductPage.class);
        CartPage cartPage = PageFactory.initElements(myDriver, CartPage.class);
        homePage.getMyAccountDropDown().click();
        homePage.getLogin().click();

        loginPage.geteMailAddressField().sendKeys("fakemail007@fakemail.com");
        loginPage.getPasswordField().sendKeys("123456789");
        loginPage.getLoginButton().click();

        myAccountPage.getYourStore().click();

        homePage.selectRandomCategory().click();
        try {
            categoryPage.getShowAll().click();
        }catch (ElementNotInteractableException en){}
        try {
            categoryPage.getContinueButton().click();
        }catch (InvalidSelectorException ie){}
        catch (NoSuchElementException nee){}
        categoryPage.getProduct().click();

        productPage.getAddToCartButton().click();
        productPage.getCheckCart().click();
        productPage.getViewCart().click();

        Assert.assertEquals(true, cartPage.getProductInCart().isDisplayed());
        Thread.sleep(5000);

        homePage.getMyAccountDropDown().click();
        homePage.getLogout().click();
        Assert.assertEquals(true, myAccountPage.getLoggedOutSuccess().isDisplayed());
        Thread.sleep(5000);
    }
}
